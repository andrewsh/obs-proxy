OBS Proxy
=========

The OBS proxy can be used to link the [Open Build Service][] backend to
workers behind a NAT or an HTTP proxy.

The proxy is split in two parts, a client and a server. The server needs to
run in the same network as the backend, while the client runs on the same
machine as the worker. One client is necessary per a worker.

Currently not supported: live log streaming.

TBC

License
=======

The project is licensed under the Mozilla Public License version 2.0.
See `COPYING.MPL` for more details.

Some individual files are licensed under different licenses, see copyright
notices in those files for more details:

 * `async_jsonrpc.py`
 * `proxied_ws.py`
 * `rpcqueue.py`

[Open Build Service]: https://openbuildservice.org/
