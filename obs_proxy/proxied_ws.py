# Proxy support for websockets
#
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2013-2018 Aymeric Augustin <aymeric.augustin@m4x.org> and contributors.
# Copyright (c)      2020 Collabora Ltd
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright notice,
#       this list of conditions and the following disclaimer in the documentation
#       and/or other materials provided with the distribution.
#     * Neither the name of websockets nor the names of its contributors may
#       be used to endorse or promote products derived from this software without
#       specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import asyncio
import collections
import functools
import logging
from typing import Any, Generator, List, Optional, Sequence, Tuple, NamedTuple, Type, cast
import urllib.request
import websockets
from websockets.client import Connect, WebSocketClientProtocol, build_authorization_basic
from websockets.exceptions import RedirectHandshake, InvalidURI
from websockets.http import Headers, HeadersLike, read_response
from websockets.uri import WebSocketURI, parse_uri

USE_SYSTEM_PROXY = object()

logger = logging.getLogger(__name__)

class ProxyURI(NamedTuple):
    """
    Proxy URI.

    :param bool secure: tells whether to connect to the proxy with TLS
    :param str host: lower-case host
    :param int port: port, always set even if it's the default
    :param str user_info: ``(username, password)`` tuple when the URI contains
      `User Information`_, else ``None``.

    .. _User Information: https://tools.ietf.org/html/rfc3986#section-3.2.1
    """

    secure: bool
    host: str
    port: int
    user_info: Optional[Tuple[str, str]]

# Work around https://bugs.python.org/issue19931

ProxyURI.secure.__doc__ = ""
ProxyURI.host.__doc__ = ""
ProxyURI.port.__doc__ = ""
ProxyURI.user_info.__doc__ = ""

def parse_proxy_uri(uri: str) -> ProxyURI:
    """
    Parse and validate an HTTP proxy URI.

    :raises ValueError: if ``uri`` isn't a valid HTTP proxy URI.

    """
    parsed = urllib.parse.urlparse(uri)
    try:
        assert parsed.scheme in ['http', 'https']
        assert parsed.hostname is not None
        assert parsed.path == '' or parsed.path == '/'
        assert parsed.params == ''
        assert parsed.query == ''
        assert parsed.fragment == ''
    except AssertionError as exc:
        raise InvalidURI(uri) from exc

    secure = parsed.scheme == 'https'
    host = parsed.hostname
    port = parsed.port or (443 if secure else 80)
    user_info = None
    if parsed.username is not None:
        # urllib.parse.urlparse accepts URLs with a username but without a
        # password. This doesn't make sense for HTTP Basic Auth credentials.
        if parsed.password is None:
            raise InvalidURI(uri)
        user_info = (parsed.username, parsed.password)
    return ProxyURI(secure, host, port, user_info)

class ProxiedWebSocketClientProtocol(WebSocketClientProtocol):
    async def proxy_connect(
        self,
        proxy_wsuri: ProxyURI,
        wsuri: WebSocketURI,
        ssl=None,
        server_hostname: Optional[str] = None
    ) -> None:
        request_headers = Headers()

        if wsuri.port == (443 if wsuri.secure else 80):  # pragma: no cover
            request_headers["Host"] = wsuri.host
        else:
            request_headers["Host"] = f'{wsuri.host}:{wsuri.port}'

        if proxy_wsuri.user_info:
            request_headers["Proxy-Authorization"] = build_authorization_basic(*proxy_wsuri.user_info)

        logger.debug("%s > CONNECT %s HTTP/1.1", self.side, f"{wsuri.host}:{wsuri.port}")
        logger.debug("%s > %r", self.side, request_headers)

        request = f'CONNECT {wsuri.host}:{wsuri.port} HTTP/1.1\r\n'
        request += str(request_headers)

        self.transport.write(request.encode())

        status_code, reason, headers = await read_response(self.reader)

        if not 200 <= status_code < 300:
            # TODO improve error handling
            raise ValueError(f"proxy error: HTTP {status_code} {reason}")

        if ssl is not None:
            transport = await asyncio.start_tls(
                self.writer.transport,
                self,
                sslcontext=None if isinstance(ssl, bool) else ssl,
                server_side=False,
                server_hostname=server_hostname
            )
            self.connection_made(transport)

class ProxiedConnect(Connect):
    """
    * ``proxy_uri`` defines the HTTP proxy for establishing the connection; by
      default, :func:`connect` uses proxies configured in the environment or
      the system (see :func:`~urllib.request.getproxies` for details); set
      ``proxy_uri`` to ``None`` to disable this behavior
    * ``proxy_ssl`` may be set to a :class:`~ssl.SSLContext` to enforce TLS
      settings for connecting to a ``https://`` proxy; it defaults to ``True``
    """

    def __init__(
        self,
        uri: str,
        *,
        create_protocol: Optional[Type[WebSocketClientProtocol]] = None,

        ping_interval: float = 20,
        ping_timeout: float = 20,
        close_timeout: Optional[float] = 10,
        max_size: int = 2 ** 20,
        max_queue: int = 2 ** 5,
        read_limit: int = 2 ** 16,
        write_limit: int = 2 ** 16,

        extra_headers: Optional[HeadersLike] = None,

        compression=None,

        proxy_uri=USE_SYSTEM_PROXY,
        proxy_ssl=None,
        **kwargs: Any,
    ) -> None:
        if create_protocol is None:
            create_protocol = ProxiedWebSocketClientProtocol

        loop = asyncio.get_event_loop()

        wsuri = parse_uri(uri)
        if wsuri.secure:
            kwargs.setdefault("ssl", True)
        elif kwargs.get("ssl") is not None:
            raise ValueError(
                "connect() received a ssl argument for a ws:// URI, "
                "use a wss:// URI to enable TLS"
            )

        if proxy_uri is USE_SYSTEM_PROXY:
            proxies = urllib.request.getproxies()
            if urllib.request.proxy_bypass(f'{wsuri.host}:{wsuri.port}'):
                proxy_uri = None
            else:
                # RFC 6455 recommends to prefer the proxy configured for HTTPS
                # connections over the proxy configured for HTTP connections.
                proxy_uri = proxies.get('https')
                if proxy_uri is None and not wsuri.secure:
                    proxy_uri = proxies.get('http')

        if proxy_uri is not None:
            proxy_uri = parse_proxy_uri(proxy_uri)
            if proxy_uri.secure:
                if proxy_ssl is None:
                    proxy_ssl = True
            elif proxy_ssl is not None:
                raise ValueError(
                    "connect() received a TLS/SSL context for a HTTP proxy; "
                    "use a HTTPS proxy to enable TLS"
                )
            conn_host, conn_port, conn_ssl = proxy_uri.host, proxy_uri.port, proxy_ssl
        else:
            conn_host, conn_port, conn_ssl = uri.host, uri.port, ssl

        kwargs["ssl"] = conn_ssl

        self._proxy_uri = proxy_uri
        self._wsuri = wsuri
        if proxy_uri is not None:
            self._ssl = kwargs.get("ssl")
            self._server_hostname = kwargs.pop('server_hostname', None)

        if compression is not None:
            raise ValueError(f"unsupported compression: {compression}")

        factory = functools.partial(
            create_protocol,
            ping_interval=ping_interval,
            ping_timeout=ping_timeout,
            close_timeout=close_timeout,
            max_size=max_size,
            max_queue=max_queue,
            read_limit=read_limit,
            write_limit=write_limit,
            loop=loop,
            host=wsuri.host,
            port=wsuri.port,
            secure=wsuri.secure,
            extra_headers=extra_headers,
        )

        create_connection = functools.partial(
            loop.create_connection,
            factory,
            conn_host,
            conn_port,
            **kwargs,
        )

        self._create_connection = create_connection

    async def __await_impl__(self) -> WebSocketClientProtocol:
        for redirects in range(self.MAX_REDIRECTS_ALLOWED):
            transport, protocol = await self._create_connection()
            # https://github.com/python/typeshed/pull/2756
            transport = cast(asyncio.Transport, transport)
            protocol = cast(WebSocketClientProtocol, protocol)

            try:
                try:
                    if self._proxy_uri is not None:
                        await protocol.proxy_connect(
                            self._proxy_uri,
                            self._wsuri,
                            self._ssl,
                            self._server_hostname,
                        )
                    await protocol.handshake(
                        self._wsuri,
                        origin=protocol.origin,
                        available_extensions=protocol.available_extensions,
                        available_subprotocols=protocol.available_subprotocols,
                        extra_headers=protocol.extra_headers,
                    )
                except Exception:
                    protocol.fail_connection()
                    await protocol.wait_closed()
                    raise
                else:
                    self.ws_client = protocol
                    return protocol
            except RedirectHandshake as exc:
                self.handle_redirect(exc.uri)
        else:
            raise SecurityError("too many redirects")

websockets.connect = ProxiedConnect

if __name__ == '__main__':
    import sys

    async def hello():
        uri = sys.argv[1]
        async with websockets.connect(uri, proxy_uri=USE_SYSTEM_PROXY) as websocket:
            while True:
                to_send = input("Data to send: ")

                await websocket.send(to_send)
                print(f"> {to_send}")

    logging.basicConfig(level=logging.DEBUG)
    asyncio.get_event_loop().run_until_complete(hello())
