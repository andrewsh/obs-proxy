#!/usr/bin/python3
#
# OBS Proxy: various utilities
#
# SPDX-License-Identifier: MPL-2.0
#
# Copyright (c) 2020 Collabora Ltd
#
# Author: Andrej Shadura <andrewsh@collabora.com>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from typing import Mapping

from jsonrpc.jsonrpc2 import JSONRPC20Request, JSONRPC20Response

from hypercorn.asyncio import serve
from hypercorn.config import Config as HyperConfig
from quart.logging import create_serving_logger

def print_ws_request(prefix, message):
    print(f"{prefix} #{message['id'] or '()'} [{message['method']}]")

def print_ws_response(prefix, message):
    if 'error' in message:
        print(f"{prefix} #{message['id'] or '()'} [{message['error']}]")
    else:
        print(f"{prefix} #{message['id'] or '()'} [{message['result']}]")

def print_ws_message(prefix, message):
    if isinstance(message, JSONRPC20Request):
        return print_ws_request(prefix, message.data)
    if isinstance(message, JSONRPC20Response):
        return print_ws_response(prefix, message.data)
    if isinstance(message, Mapping):
        if 'method' in message:
            return print_ws_request(prefix, message)
        if 'error' in message or 'result' in message:
            return print_ws_response(prefix, message)
    raise TypeError("message type unsupported", message)

def run_multiserver(
    app,
    host='127.0.0.1',
    https_ports=[6001],
    ports=[],
    debug=None,
    use_reloader=True,
    ca_certs=None,
    certfile=None,
    keyfile=None,
    shutdown_trigger=None
):
    """
    Run a Quart app on multiple ports using Hypercorn
    """
    config = HyperConfig()
    config.access_log_format = "%(h)s %(r)s %(s)s %(b)s %(D)s"
    config.accesslog = create_serving_logger()
    config.bind = [f'{host}:{port}' for port in https_ports] or [
        f'{host}:{port}' for port in ports
    ]
    config.insecure_bind = [f"{host}:{port}" for port in ports]
    config.ca_certs = ca_certs if https_ports else None
    config.certfile = certfile if https_ports else None
    if debug is not None:
        app.debug = debug
    config.errorlog = config.accesslog
    config.keyfile = keyfile if https_ports else None
    config.use_reloader = use_reloader
    return serve(app, config, shutdown_trigger=shutdown_trigger)

def filterdict(d, keep=None, remove=None):
    """
    Filter a dict removing keys in `remove` and keeping keys in `keep`
    """
    return {
        k: v for k, v in d.items()
             if (not remove or k not in remove) and
                (not keep or k in keep)
    }
