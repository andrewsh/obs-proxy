# OBS Proxy: worker dataclasses
#
# SPDX-License-Identifier: MPL-2.0
#
# Copyright (c) 2020 Collabora Ltd
#
# Author: Andrej Shadura <andrewsh@collabora.com>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import asyncio
from dataclasses import dataclass, field, fields, asdict
import xmltodict

@dataclass
class Worker:
    port: int
    hostarch: str
    ip: str = None
    meta: dict = field(default_factory=dict)
    workerid: str = None

    def __post_init__(self):
        if not self.workerid:
            self.workerid = f"{self.ip}:{self.port}"

    def __hash__(self):
        return hash(self.workerid)

    def asxml(self, pure=False, meta=True):
        meta = self.meta.copy() if meta else {}
        meta.update({
            '@port': self.port,
            '@workerid': self.workerid,
            '@hostarch': self.hostarch,
        } if pure else {
            '@ip': self.ip,
            '@port': self.port,
            '@workerid': self.workerid,
            '@hostarch': self.hostarch,
        })

        return xmltodict.unparse({
            'worker': meta,
        }, pretty=True)

    def asdict(self):
        return asdict(self)

    def update(self, worker):
        if worker.ip:
            self.ip = worker.ip
        self.port = worker.port
        self.hostarch = worker.hostarch
        if worker.meta:
            self.meta = worker.meta

    @classmethod
    def fromdict(cls, d):
        return cls(**d)

    @classmethod
    def fromxml(cls, xml, **extra):
        data = xmltodict.parse(xml)['worker']
        new = {
            k.name: data[f'@{k.name}']
            for k in fields(cls)
            if f'@{k.name}' in data
        }
        new.update(meta={k: data[k] for k in data if not k.startswith('@')})
        new.update(extra)
        return cls(**new)

@dataclass
class ProxiedWorker(Worker):
    externalport: int = None
    state: str = None

    def __post_init__(self):
        self.queue = asyncio.Queue()

    def __hash__(self):
        return hash(self.workerid)

    def internal(self):
        return self

    def external(self):
        return Worker(
            ip=self.ip,
            port=self.externalport,
            hostarch=self.hostarch,
            meta=self.meta,
            workerid=self.workerid
        )
