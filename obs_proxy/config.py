#!/usr/bin/python3
#
# OBS Proxy: configuration
#
# SPDX-License-Identifier: MPL-2.0
#
# Copyright (c) 2020 Collabora Ltd
#
# Author: Andrej Shadura <andrewsh@collabora.com>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from dataclasses import dataclass, field, fields, asdict
from configparser import ConfigParser
from pathlib import Path
from typing import Iterable

@dataclass
class ServerConfig:
    host: str
    port: int
    http2: bool
    tls: bool
    keyfile: Path = None
    certfile: Path = None


@dataclass
class ClientConfig:
    host: str
    port: int
    proxy: str


@dataclass
class AuthConfig:
    username: str
    password: str
    token: str


@dataclass
class BackendConfig:
    srcserver_uri: str
    repserver_uri: str


def path_or_none(p: Path):
    return Path(p) if p else None


@dataclass
class Config:
    server: ServerConfig
    client: ClientConfig
    auth: AuthConfig
    backend: BackendConfig
    worker_ports: Iterable[int]

    @classmethod
    def parse_file(cls, f):
        parser = ConfigParser()
        print(f"Parsing {f}")
        parser.read(f)
        startport = parser["workers"].getint("startport")
        endport = parser["workers"].getint("endport")
        worker_ports = range(startport, endport + 1)

        server = {
            "host": parser["server"].get("host"),
            "port": parser["server"].getint("port", 6000),
            "http2": parser["server"].getboolean("http2", False),
            "tls": parser["server"].getboolean("tls", False),
            "keyfile": path_or_none(parser["server"]["keyfile"]),
            "certfile": path_or_none(parser["server"]["certfile"]),
        }

        client = {
            "host": parser["client"].get("host"),
            "port": parser["client"].getint("port", 5000),
            "proxy": parser["client"].get("proxy", None),
        }

        auth = {
            "username": parser["auth"].get("username"),
            "password": parser["auth"].get("password"),
            "token": parser["auth"].get("token"),
        }

        srcserver_host, srcserver_port = parser["backend"]["srcserver"].split(":")
        repserver_host, repserver_port = parser["backend"]["repserver"].split(":")
        backend_host = parser["backend"]["host"]

        backend = {
            "srcserver_uri": "http://%s:%s" % (srcserver_host or backend_host, srcserver_port),
            "repserver_uri": "http://%s:%s" % (repserver_host or backend_host, repserver_port),
        }

        return cls(
            server=ServerConfig(**server),
            client=ClientConfig(**client),
            auth=AuthConfig(**auth),
            backend=BackendConfig(**backend),
            worker_ports=worker_ports,
        )


def find_config() -> Path:
    appname = 'obs-proxy'
    for d in (f'/etc/{appname}', f'/usr/lib/{appname}', f'~/.config/{appname}', '.'):
        p = Path(d) / 'proxy.conf'
        if p.is_file():
            return p
    raise FileNotFoundError("Can't find proxy.conf")

config = Config.parse_file(find_config())
