#!/usr/bin/python3
#
# OBS Proxy: client side
# RepoServer/SourceServer API proxy
#
# SPDX-License-Identifier: MPL-2.0
#
# Copyright (c) 2020 Collabora Ltd
#
# Author: Andrej Shadura <andrewsh@collabora.com>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import asyncio
from typing import Optional

from jsonrpc.jsonrpc import JSONRPCRequest, JSONRPC20Request
import httpx
from quart import Quart, redirect, request, url_for, websocket, make_response
import xmltodict

from .config import config
from .utils import filterdict
from .worker import Worker
from .wsclient import queue

HTTP_TIMEOUT = 5 * 60

# Set upon a first connect
worker = None

repserver = Quart(__name__)

# Raised when the worker connects for the first time.
# The worker tells the proxy client its name so that
# it can we used when establishing the WebSocket link.
first_connect = asyncio.Event()

client = httpx.AsyncClient(
    http2=config.server.http2,
    verify=False, # TODO
    proxies={
        'all': config.client.proxy
    } if config.client.proxy else None
)

def proxy_server_uri(path: str, upstream: str = "worker", worker_id: Optional[str] = None) -> str:
    if not worker_id:
        worker_id = worker.workerid if worker else '-'
    return f"{'https' if config.server.tls else 'http'}://{config.server.host}:{config.server.port}/{upstream}/{worker_id}/{path}"

def build_auth():
    if config.auth.username:
        return config.auth.username, config.auth.password
    else:
        return lambda req: req.headers.update({
            "Authorization": f"Bearer {config.auth.token}"
        })


@repserver.route('/getbuildcode')
@repserver.route('/getworkercode')
@repserver.route('/<upstream>/<path:worker_id>/badpackagebinaryversionlist')
@repserver.route('/<upstream>/<path:worker_id>/build/<project>/<repository>/<arch>/<package>')
@repserver.route('/<upstream>/<path:worker_id>/getbinaries')
@repserver.route('/<upstream>/<path:worker_id>/getbinaryversions')
@repserver.route('/<upstream>/<path:worker_id>/getbuildcode')
@repserver.route('/<upstream>/<path:worker_id>/getconfig')
@repserver.route('/<upstream>/<path:worker_id>/getjobdata')
@repserver.route('/<upstream>/<path:worker_id>/getobsgendiffdata')
@repserver.route('/<upstream>/<path:worker_id>/getpackagebinaryversionlist')
@repserver.route('/<upstream>/<path:worker_id>/getpreinstallimageinfos')
@repserver.route('/<upstream>/<path:worker_id>/getsources')
@repserver.route('/<upstream>/<path:worker_id>/getsslcert')
@repserver.route('/<upstream>/<path:worker_id>/getworkercode')
@repserver.route('/<upstream>/<path:worker_id>/source/<project>')
@repserver.route('/<upstream>/<path:worker_id>/source/<project>/<package>')
async def proxy_request(upstream=None, worker_id=None, **kv):
    path_parts = request.path.split('/')
    if upstream:
        uri = proxy_server_uri('/'.join(path_parts[3:]), upstream, worker_id)
    else:
        uri = proxy_server_uri(path_parts[1], 'repserver')

    print(f"Proxying {uri}")

    async def async_generator(resp):
        async for chunk in resp.aiter_bytes():
            yield chunk
        await resp.aclose()

    params = dict(request.args)
    if params.get('nometa') == '':
        params['nometa'] = 1

    req = client.build_request('GET', uri, params=params)
    try:
        resp = await client.send(req, stream=True, auth=build_auth(), timeout=HTTP_TIMEOUT)
    except httpx.HTTPError as e:
        return str(e), 500
    return async_generator(resp), resp.status_code, filterdict(resp.headers, ['content-type', 'content-length', 'cache-control'])

@repserver.route('/putjob', methods=['POST'])
@repserver.route('/repserver/<path:worker_id>/putjob', methods=['POST'])
async def putjob(worker_id=None, **kv):
    uri = proxy_server_uri('putjob', 'repserver', worker_id)
    print(f"Proxying {uri}")

    async def upload_bytes():
        async for data in request.body:
            yield data

    try:
        resp = await client.post(
            uri, params=request.args, data=upload_bytes(), auth=build_auth(), timeout=HTTP_TIMEOUT
        )
    except httpx.HTTPError as e:
        return str(e), 500
    return "ok"

@repserver.route('/worker', methods=['POST', 'GET'])
@repserver.route('/repserver/<path:worker_id>/worker', methods=['POST', 'GET'])
async def worker_ping(worker_id=None):
    global worker
    data = await request.get_data()
    state = request.args.get('state')
    peer_port = request.args.get('port')
    host_arch = request.args.get('arch')
    worker_id = request.args.get('workerid', worker_id or f"{request.remote_addr}:{peer_port}")
    if data:
        worker = Worker.fromxml(
            data,
            ip=request.remote_addr,
            port=peer_port,
            workerid=worker_id,
            hostarch=host_arch,
        )
        print(f"    worker data: {worker}")
        first_connect.set()
    else:
        worker.port = peer_port
        worker.hostarch = host_arch
    print(f"    {worker.hostarch}:{worker.workerid} {state} on {worker.port}")
    queue.put_nowait(
        JSONRPC20Request(
            method='worker_ping', params={'worker': worker.asdict(), 'state': state}
        )
    )
    return 'ok'
