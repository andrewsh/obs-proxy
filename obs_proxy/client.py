#!/usr/bin/python3
#
# OBS Proxy: client side
#
# SPDX-License-Identifier: MPL-2.0
#
# Copyright (c) 2020 Collabora Ltd
#
# Author: Andrej Shadura <andrewsh@collabora.com>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import asyncio
import signal
import os

from .repserver import repserver
from .wsclient import wsclient, queue
from .utils import run_multiserver

from .config import config

def run():
    loop = asyncio.get_event_loop()
    shutdown_event = asyncio.Event()

    def _terminate():
        print("Terminating.")
        os._exit(0)

    def _signal_handler(*e):
        shutdown_event.set()
        print(f"Received a signal, will terminate")
        loop.call_later(1, _terminate)

    try:
        loop.add_signal_handler(signal.SIGTERM, _signal_handler)
        loop.add_signal_handler(signal.SIGINT, _signal_handler)
    except (AttributeError, NotImplementedError):
        pass

    worker = run_multiserver(
        repserver,
        host='0.0.0.0',
        https_ports=[],
        ports=[config.client.port],
        shutdown_trigger=shutdown_event.wait,
        debug=False
    )
    wsworker = wsclient(queue, shutdown_trigger=shutdown_event.wait)
    tasks = asyncio.gather(worker, wsworker)

    try:
        loop.run_until_complete(tasks)
    except Exception as e:
        print(e)
    finally:
        try:
            tasks = [task for task in asyncio.all_tasks(loop) if not task.done()]
            print(f"{tasks = }")
            if tasks:
                for task in tasks:
                    task.cancel()
                loop.run_until_complete(
                    asyncio.gather(*tasks, loop=loop, return_exceptions=True)
                )
                loop.run_until_complete(loop.shutdown_asyncgens())
        finally:
            asyncio.set_event_loop(None)
            loop.close()

if __name__ == '__main__':
    run()
