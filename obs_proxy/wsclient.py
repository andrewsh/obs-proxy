#!/usr/bin/python3
#
# OBS Proxy: client side
# WebSocket client and related handling
#
# SPDX-License-Identifier: MPL-2.0
#
# Copyright (c) 2020 Collabora Ltd
#
# Author: Andrej Shadura <andrewsh@collabora.com>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import asyncio
import hashlib
import json
import ssl
import websockets
import httpx
import xmltodict
from hypercorn.utils import raise_shutdown

from jsonrpc.jsonrpc import JSONRPCRequest, JSONRPC20Request

from . import proxied_ws
from .async_jsonrpc import AsyncJSONRPCResponseManager, dispatcher
from .config import config
from .utils import print_ws_message
from .worker import Worker

import random
random.seed()

queue = asyncio.Queue()

client = httpx.AsyncClient(http2=False)

def worker_uri(worker, action):
    return f'http://{worker.ip}:{worker.port}/{action}'

@dispatcher.add_method
async def worker_action(action=None, jobid=None):
    from .repserver import worker
    if worker:
        uri = worker_uri(worker, action)
        try:
            print(f"==> [{action}]")
            resp = await client.get(uri, params={'jobid': jobid} if jobid else None)
        except httpx.HTTPError as e:
            raise Exception(f"HTTP error: {e}", uri)
        print(f"<== [{resp.status_code} {resp.reason_phrase}]")
        return {'content': resp.text, 'code': resp.status_code}
    else:
        raise Exception("No worker")

@dispatcher.add_method
async def worker_info(jobid=None):
    from .repserver import worker
    if worker:
        action = 'worker'
        uri = worker_uri(worker, action)
        try:
            print(f"==> [{action}]")
            resp = await client.get(uri, params={'jobid': jobid} if jobid else None)
        except httpx.HTTPError as e:
            raise Exception(f"HTTP error: {e}", uri)
        if resp.status_code == 200:
            # update cached data
            w = Worker.fromxml(resp.text)
            worker.update(w)
        print(f"<== [{resp.status_code} {resp.reason_phrase}]")
        return {
            'worker': worker.asdict(),
            'code': resp.status_code,
            'reason': resp.reason_phrase
        }
    else:
        raise Exception("No worker")

@dispatcher.add_method
async def submit_job(job=None, jobid=None, extra={}):
    from .repserver import worker
    if worker:
        uri = worker_uri(worker, 'build')
        try:
            extra.update({'port': config.client.port})
            job['buildinfo'].update({
                "@srcserver": f"http://{config.client.host}:{config.client.port}/srcserver/{worker.workerid}",
                "@reposerver": f"http://{config.client.host}:{config.client.port}/repserver/{worker.workerid}",
            })
            for path in job['buildinfo']['path']:
                if path['@server'] == config.backend.srcserver_uri:
                    path['@server'] = f"http://{config.client.host}:{config.client.port}/srcserver/{worker.workerid}"
                if path['@server'] == config.backend.repserver_uri:
                    path['@server'] = f"http://{config.client.host}:{config.client.port}/repserver/{worker.workerid}"

            job_xml = xmltodict.unparse(job).encode('utf-8')
            if jobid:
                new_jobid = hashlib.md5(job_xml).hexdigest()
                print(f"==> [build {jobid} => {new_jobid}]")
                queue.put_nowait(
                    JSONRPC20Request(
                        method='job_started',
                        params={'orig_job_id': jobid, 'job_id': new_jobid}
                    )
                )
            resp = await client.put(uri, params=extra, data=job_xml)
        except httpx.HTTPError as e:
            raise Exception(f"HTTP error: {e}", uri)
        print(f"<== [{resp.status_code} {resp.reason_phrase}]")
        return {'content': resp.text, 'code': resp.status_code}
    else:
        raise Exception("No worker")

async def consumer_handler(websocket):
    async for data in websocket:
        try:
            message = json.loads(data)
        except (TypeError, ValueError) as e:
            print(f"### invalid JSON of length {len(data)}")
            continue
        try:
            if 'method' in message:
                print_ws_message('>>>', message)
                response = await AsyncJSONRPCResponseManager.handle(message, dispatcher)
                print_ws_message('<<-', response)
                await websocket.send(response.json)
            else:
                print(f"### Unknown message: {message = }")
        except e:
            print(f"### {e = }")

async def producer_handler(websocket):
    while True:
        message = await queue.get()
        print_ws_message('<<<', message)
        await websocket.send(message.json)

async def wsclient(queue, shutdown_trigger=None):
    from .repserver import first_connect

    print(f"Waiting for the worker to connect")
    await first_connect.wait()

    from .repserver import worker

    if config.auth.username:
        auth_prefix = f"{config.auth.username}:{config.auth.password}@"
        extra_headers = {}
        explanation = f", authenticated with a password as {config.auth.username}"
    else:
        auth_prefix = ""
        extra_headers = {"Authorization": f"Bearer {config.auth.token}"}
        explanation = f", authenticated with a token"
    scheme = 'wss' if config.server.tls else 'ws'
    uri = f'{scheme}://{auth_prefix}{config.server.host}:{config.server.port}/worker/{worker.workerid}/events'
    while True:
        timeout = random.randrange(2, 10)
        try:
            async with websockets.connect(
                uri,
                compression=None,
                proxy_uri=config.client.proxy,
                extra_headers=extra_headers
            ) as websocket:
                print("Connected to {scheme}://{config.server.host}:{config.server.port} using WebSockets{explanation}")
                if worker and not first_connect.is_set():
                    print(f"Bootstrapping the worker")
                    queue.put_nowait(
                        JSONRPC20Request(
                            method='worker_ping',
                            params={'worker': worker.asdict(), 'state': 'idle'}
                        )
                    )
                consumer_task = asyncio.ensure_future(consumer_handler(websocket))
                producer_task = asyncio.ensure_future(producer_handler(websocket))
                shutdown_task = asyncio.create_task(raise_shutdown(shutdown_trigger))
                done, pending = await asyncio.wait(
                    [consumer_task, producer_task, shutdown_task],
                    return_when=asyncio.FIRST_COMPLETED,
                )
                for task in [consumer_task, producer_task]:
                    task.cancel()
                if shutdown_task in done:
                    return
        except (websockets.exceptions.WebSocketException, OSError) as e:
            print("Caught an exception:")
            print(f"{e = }")
            print(f"Waiting for {timeout} seconds before reconnecting")
            await asyncio.sleep(timeout)
            first_connect.clear()
            continue
