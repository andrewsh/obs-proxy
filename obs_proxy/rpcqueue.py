#!/usr/bin/python3
#
# OBS Proxy: RPC queue
# Keeps track of sent JSON-RPC queries and matches them to the responses
#
# SPDX-License-Identifier: MIT
#
# Copyright (c) 2020 Collabora Ltd
#
# Author: Andrej Shadura <andrewsh@collabora.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import asyncio
from asyncio import events, locks

class RPCQueue:
    """
    Keeps track of JSON-RPC queries and matches responses, when they arrive,
    back to them, allowing to await the response directly where the call
    is performed.

    Each query is represented by a future addressable by the call’s id;
    if id is None, a sequential integer is generated. The generated id
    is always greater than the id in the last message sent.

    An internal queue for unsent queries is maintained for a transmitting
    coroutine to pick up messages from.
    """
    def __init__(self):
        self._loop = events.get_event_loop()
        self._callers = dict()
        self._finished = locks.Event()
        self._finished.set()
        self._last_id = 0
        self._queue = asyncio.Queue()

    def __repr__(self):
        return f'<{type(self).__name__} at {id(self):#x} {self._format()}>'

    def __str__(self):
        return f'<{type(self).__name__} {self._format()}>'

    def _next_id(self, _id):
        """
        Generate a new id when none is passed, always greater than the last one.
        """
        self._last_id = _id or (self._last_id + 1)
        return self._last_id

    def _format(self):
        return f"{self._callers!r}"

    def get(self):
        """
        Get the next message for transmission.

        Intended to be called by the coroutine pushing messages into the actual
        communication channel.
        """
        return self._queue.get()

    async def call(self, message, func=None):
        """
        Submit a message and wait for a response.

        The message object must have _id and method attributes, either can be None.

        If func is passed, the function is awaited to transmit the message,
        otherwise it is put into an internal queue.
        """
        _id = self._next_id(message._id)
        message._id = _id
        if func:
            await func(message)
        else:
            await self._queue.put(message)
            # TODO: proper logging
            print(f"<<< #{message._id or '<>'} [{message.method}]")
        while True:
            caller = self._loop.create_future()
            self._callers[_id] = caller
            try:
                return await caller
            except Exception as e:
                caller.cancel()
                if self._callers.get(_id) == caller:
                    del self._callers[_id]
                raise

    async def reply(self, message):
        """
        Register a response and notify the awaiting coroutine that
        it can resume.
        """
        _id = message._id
        caller = self._callers.pop(_id, None)
        if caller and not caller.done():
            caller.set_result(message)

async def main():
    import dataclasses

    @dataclasses.dataclass
    class FauxMsg:
        _id: int
        msg: str

    async def caller(q):
        async def t(x):
            print(f"{x = }")
        resp = await q.call(FauxMsg(_id=None, msg="Test"), func=t)
        print(f"{resp = }")

    async def replier(q):
        await asyncio.sleep(3)
        print("slept")
        await q.reply(FauxMsg(_id=1, msg="hey"))
        print("replied")

    q = RPCQueue()
    loop = asyncio.get_running_loop()
    loop.create_task(caller(q))
    print(f"{q = }")
    loop.create_task(replier(q))
    await asyncio.sleep(10)

if __name__ == '__main__':
    asyncio.run(main())
