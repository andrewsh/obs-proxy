#!/usr/bin/python3
#
# OBS Proxy: server side
#
# SPDX-License-Identifier: MPL-2.0
#
# Copyright (c) 2020 Collabora Ltd
#
# Author: Andrej Shadura <andrewsh@collabora.com>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import asyncio
import functools
import hashlib
import json
import logging
from logging import debug, info
import os
import signal
import sys
from typing import Optional

from quart import (
    Quart,
    request,
    websocket,
    abort,
    has_request_context
)
from bidict import bidict
import httpx
import xmltodict

from jsonrpc.jsonrpc2 import JSONRPC20Request, JSONRPC20Response
from .async_jsonrpc import AsyncJSONRPCResponseManager, dispatcher

from .config import config
from .rpcqueue import RPCQueue
from .utils import filterdict, run_multiserver, print_ws_message
from .worker import Worker, ProxiedWorker

logger = logging.getLogger(__name__)

app = Quart(__name__)

client = httpx.AsyncClient()

workers = bidict()

ports = bidict()

ws_connections = {}

jobs = bidict()

def require_auth(f):
    @functools.wraps(f)
    async def wrapper(*args, **kwargs):
        ctx = request if has_request_context() else websocket
        _, port = ctx.host.split(':')
        if int(port) == config.server.port:
            auth_header = ctx.headers.get("Authorization")
            if not auth_header or not (
                auth_header.startswith("Basic") or auth_header.startswith("Bearer")
            ):
                return 'auth required', 401, {'WWW-Authenticate': 'Basic'}

            # Quart parses Basic auth for us
            if ctx.authorization:
                if (
                    ctx.authorization["username"] != config.auth.username
                    or ctx.authorization["password"] != config.auth.password
                ):
                    debug(f"{ctx.authorization = }, {config.auth = }")
                    abort(403)
            else:
                _, token = auth_header.split(' ', maxsplit=1)
                if token != config.auth.token:
                    debug(f"{auth_header = }, {config.auth = }")
                    abort(403)

        return await f(*args, **kwargs)

    return wrapper


def handout_port():
    return next(port for port in ports.keys() if ports[port] is None)


def find_worker(worker_id):
    if worker_id:
        return worker_id
    _, port = request.host.split(':')
    if not worker_id and ports.get(int(port)):
        worker_id = ports[int(port)].workerid
        debug(f"detected {worker_id = }")
    return worker_id


def per_worker(f):
    @functools.wraps(f)
    async def wrapper(worker_id: Optional[str] = None, *args, **kwargs):
        worker_id = find_worker(worker_id)
        return await f(worker_id, *args, **kwargs)

    return wrapper


@dispatcher.add_method
async def worker_ping(worker, state):
    w = ProxiedWorker.fromdict(worker)
    if w.workerid not in workers:
        w.externalport = handout_port()
        print(f"    alloc {w.externalport}")
        workers[w.workerid] = w
        ports[w.externalport] = w
    else:
        workers[w.workerid].update(w)
    workers[w.workerid].state = state

    w = workers[w.workerid]
    xml = w.external().asxml(pure=True)
    params = {
        'state': state,
        'arch': w.hostarch,
        'port': w.externalport,
        'workerid': w.workerid
    }
    print(f"    {w.hostarch}:{w.workerid} {state} on {w.port}")
    try:
        resp = await client.post(
            f"{config.backend.repserver_uri}/worker", params=params, data=xml
        )
        print(f"==> [worker_ping]")
    except httpx.HTTPError as e:
        print(f"<!= [worker_pong {resp.status_code} {resp.reason_phrase}]")
        raise Exception(f"HTTP error: {e}")
    print(f"<== [worker_pong {resp.status_code} {resp.reason_phrase}]")
    return (
        'ok' if resp.status_code == 200 else f"{resp.status_code} {resp.reason_phrase}"
    )


@dispatcher.add_method
async def job_started(orig_job_id, job_id):
    jobs[job_id] = orig_job_id
    return 'ok'


async def ws_send(inq, outq):
    while True:
        message = await inq.get()
        await websocket.send(message.json)


async def ws_receive(inq, outq):
    while True:
        data = await websocket.receive()
        try:
            message = json.loads(data)
        except (TypeError, ValueError):
            continue
        if 'method' in message:
            response = await AsyncJSONRPCResponseManager.handle(data, dispatcher)
            await websocket.send(response.json)
        else:
            if 'id' in message and ('result' in message or 'error' in message):
                response = JSONRPC20Response(_id=message['id'], **message)
                await outq.reply(response)

@app.route('/logfile')
async def logfile():
    pass


@app.route('/worker/<path:worker_id>/ping', methods=['POST'])
async def worker_ping_http(worker_id: str, state=None):
    data = await request.get_json()
    return worker_ping(data, state)


@app.websocket('/worker/<path:worker_id>/events')
@require_auth
async def worker_events_ws(worker_id: str):
    async def send(queue):
        while True:
            message = await queue.get()
            await websocket.send(message.json)

    async def recv(queue):
        while True:
            print(f"... awaiting")
            data = await websocket.receive()
            try:
                message = json.loads(data)
            except (TypeError, ValueError) as e:
                print(f"### invalid JSON of length {len(data)}")
                continue
            if 'id' in message and ('result' in message or 'error' in message):
                print_ws_message('->>', message)
                response = JSONRPC20Response(_id=message['id'], **message)
                await queue.reply(response)
            elif 'method' in message:
                print_ws_message('>>>', message)
                response = await AsyncJSONRPCResponseManager.handle(data, dispatcher)
                print_ws_message('<x-', response)

    print(f"(o) {worker_id} online")
    queue = RPCQueue()
    ws_connections[worker_id] = queue
    try:
        producer = asyncio.create_task(send(queue))
        consumer = asyncio.create_task(recv(queue))
        await asyncio.gather(producer, consumer)
    finally:
        ws_connections.pop(worker_id)

@app.route('/worker/<path:worker_id>/events')
@require_auth
async def worker_events(worker_id: str):
    return "Use WebSockets", 500


@app.route('/info')
@app.route('/worker/<path:worker_id>/buildinfo')
@require_auth
@per_worker
async def worker_buildinfo(worker_id: str = None):
    print(f"not implemented: {request.args}")
    return "not implemented", 500


@app.route('/worker')
@app.route('/worker/<path:worker_id>/info')
@require_auth
@per_worker
async def worker_info(worker_id: str = None):
    if worker_id not in ws_connections:
        return 'not found', 404
    params = dict(request.args)
    if params.get('jobid') in jobs:
        print(f"found job mapping for {params['jobid']}")
        params['jobid'] = jobs[params['jobid']]
        print(f"mapping to the orig job id {params['jobid']}")

    resp = await ws_connections[worker_id].call(JSONRPC20Request(method='worker_info'))
    if resp.error:
        return resp.error, 500
    w = Worker.fromdict(resp.result['worker'])
    workers[worker_id].update(w)
    w = workers[worker_id]
    return f"{w.external().asxml(pure=True, meta=False)}", {'content-type': 'text/xml'}


@app.route('/worker/<path:worker_id>/status')
@require_auth
@per_worker
async def worker_status(worker_id: str = None):
    return workers[worker_id].asdict() if worker_id in workers else ('not found', 404)


@app.route('/logfile', methods=['GET'])
@app.route('/worker/<path:worker_id>/logfile', methods=['POST', 'GET'])
@require_auth
@per_worker
async def post_logfile(worker_id: str = None):
    print(f"{request.host = }")
    print(f"{request.args = }")
    return 'ok'


@app.route('/kill')
@app.route('/discard')
@app.route('/badhost')
@app.route('/sysrq')
@app.route('/worker/<path:worker_id>/kill')
@app.route('/worker/<path:worker_id>/discard')
@app.route('/worker/<path:worker_id>/badhost')
@app.route('/worker/<path:worker_id>/sysrq')
@require_auth
@per_worker
async def get_action(worker_id: str):
    worker_id = find_worker(worker_id)
    if worker_id not in ws_connections:
        return 'not found', 404
    job_id = request.args.get('jobid', None)
    resp = await ws_connections[worker_id].call(
        JSONRPC20Request(
            method='worker_action',
            params={'action': request.path.split('/')[-1], 'jobid': job_id}
        )
    )
    if resp.error:
        return resp.error, 500
    else:
        return resp.result['content'], resp.result['code']


@app.route('/repserver/<path:worker_id>/putjob', methods=['POST'])
@app.route('/repserver/<path:worker_id>/workerdispatched', methods=['POST'])
@require_auth
async def worker_putjob(worker_id: str = None):
    if worker_id not in ws_connections:
        return 'not found', 404
    params = dict(request.args)
    if params.get('jobid') in jobs:
        print(f"    found job mapping for {params['jobid']}")
        params['jobid'] = jobs.pop(params['jobid'])
        print(f"    mapping to the orig job id {params['jobid']}")

    uri = config.backend.repserver_uri + '/' + '/'.join(request.path.split('/')[3:])

    async def upload_bytes():
        async for data in request.body:
            yield data

    try:
        resp = await client.post(uri, params=params, data=upload_bytes())
    except httpx.HTTPError as e:
        return str(e), 500
    print(f"    putjob {resp = }")
    print(resp.content)
    return "ok"


@app.route('/build', methods=['PUT'])
@app.route('/worker/<path:worker_id>/build', methods=['PUT'])
@require_auth
@per_worker
async def worker_build(worker_id: str = None):
    if worker_id not in ws_connections:
        return 'not found', 404
    job_xml = await request.get_data()
    job_id = hashlib.md5(job_xml).hexdigest()
    job_data = xmltodict.parse(job_xml)
    resp = await ws_connections[worker_id].call(
        JSONRPC20Request(
            method='submit_job',
            params={'extra': request.args, 'job': job_data, 'jobid': job_id}
        )
    )
    if resp.error:
        return resp.error, 500
    else:
        return resp.result['content'], resp.result['code']


@app.route('/repserver/<path:worker_id>/badpackagebinaryversionlist')
@app.route('/repserver/<path:worker_id>/getbinaries')
@app.route('/repserver/<path:worker_id>/getbinaryversions')
@app.route('/repserver/<path:worker_id>/getbuildcode')
@app.route('/repserver/<path:worker_id>/getjobdata')
@app.route('/repserver/<path:worker_id>/getpackagebinaryversionlist')
@app.route('/repserver/<path:worker_id>/getpreinstallimageinfos')
@app.route('/repserver/<path:worker_id>/getworkercode')
@app.route('/repserver/<path:worker_id>/build/<project>/<repository>/<arch>/<package>')
@require_auth
async def proxy_rep_request(worker_id: str, **kv):
    uri = config.backend.repserver_uri + '/' + '/'.join(request.path.split('/')[3:])
    print(f"Proxying {uri}")

    async def async_generator(response):
        async for chunk in response.aiter_bytes():
            yield chunk
        await response.aclose()

    req = client.build_request('GET', uri, params=request.args)
    try:
        resp = await client.send(req, stream=True)
    except httpx.HTTPError as e:
        return str(e), 500
    return (
        async_generator(resp),
        resp.status_code,
        filterdict(resp.headers, ['content-type', 'content-length', 'cache-control'])
    )


@app.route('/srcserver/<path:worker_id>/getbinaries')
@app.route('/srcserver/<path:worker_id>/getbinaryversions')
@app.route('/srcserver/<path:worker_id>/getconfig')
@app.route('/srcserver/<path:worker_id>/getobsgendiffdata')
@app.route('/srcserver/<path:worker_id>/getsources')
@app.route('/srcserver/<path:worker_id>/getsslcert')
@app.route('/srcserver/<path:worker_id>/build/<project>/<repository>/<arch>/<package>')
@app.route('/srcserver/<path:worker_id>/source/<project>')
@app.route('/srcserver/<path:worker_id>/source/<project>/<package>')
@require_auth
async def proxy_src_request(worker_id: str, **kv):
    uri = config.backend.srcserver_uri + '/' + '/'.join(request.path.split('/')[3:])
    print(f"Proxying {uri}")

    async def async_generator(response):
        async for chunk in response.aiter_bytes():
            yield chunk
        await response.aclose()

    req = client.build_request('GET', uri, params=request.args)
    try:
        resp = await client.send(req, stream=True)
    except httpx.HTTPError as e:
        return str(e), 500
    return (
        async_generator(resp),
        resp.status_code,
        filterdict(resp.headers, ['content-type', 'content-length', 'cache-control'])
    )


def run():
    global ports

    ports = {port: None for port in config.worker_ports}

    print(f"Proxying for srcserver {config.backend.srcserver_uri} and repserver {config.backend.repserver_uri}")

    loop = asyncio.get_event_loop()
    shutdown_event = asyncio.Event()

    def _terminate():
        print("Terminating.")
        os._exit(0)

    def _signal_handler(*e):
        shutdown_event.set()
        print(f"Received a signal, will terminate")
        loop.call_later(1, _terminate)

    try:
        loop.add_signal_handler(signal.SIGTERM, _signal_handler)
        loop.add_signal_handler(signal.SIGINT, _signal_handler)
        pass
    except (AttributeError, NotImplementedError):
        pass

    if config.server.tls:
        worker = run_multiserver(
            app,
            host=config.server.host,
            https_ports=[config.server.port],
            ports=[*ports.keys()],
            shutdown_trigger=shutdown_event.wait,
            debug=False,
            certfile=config.server.certfile,
            keyfile=config.server.keyfile
        )
    else:
        worker = run_multiserver(
            app,
            host=config.server.host,
            https_ports=[],
            ports=[config.server.port, *ports.keys()],
            shutdown_trigger=shutdown_event.wait,
            debug=False
        )
    print(f"Will listen on worker ports {config.worker_ports[0]} to {config.worker_ports[-1]}")

    try:
        loop.run_until_complete(worker)
    except Exception as e:
        print("Caught an exception:")
        print(e)
        sys.exit(1)
    finally:
        try:
            tasks = [task for task in asyncio.all_tasks(loop) if not task.done()]
            print(f"{tasks = }")
            if tasks:
                for task in tasks:
                    task.cancel()
                loop.run_until_complete(
                    asyncio.gather(*tasks, loop=loop, return_exceptions=True)
                )
                loop.run_until_complete(loop.shutdown_asyncgens())
        finally:
            asyncio.set_event_loop(None)
            loop.close()


if __name__ == '__main__':
    run()
